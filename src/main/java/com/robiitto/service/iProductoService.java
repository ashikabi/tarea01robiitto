package com.robiitto.service;

import java.util.List;

import com.robiitto.model.*;

public interface iProductoService {

	Producto registrar(Producto producto);
	
	void modificar(Producto producto);
	
	void eliminar(int idProducto);
	
	List<Producto> listar();
	
}
