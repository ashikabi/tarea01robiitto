package com.robiitto.service;

import java.util.List;

import com.robiitto.model.DetalleVenta;

public interface iDetalleVentaService {

	DetalleVenta registrar(DetalleVenta detalleVenta);
	
	DetalleVenta listarId(int idDetalleVenta);
	
	List<DetalleVenta> listar();
	
}
