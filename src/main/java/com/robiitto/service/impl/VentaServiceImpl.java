package com.robiitto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robiitto.dao.iVentaDAO;
import com.robiitto.model.Venta;
import com.robiitto.service.iVentaService;

@Service
public class VentaServiceImpl implements iVentaService{

	@Autowired
	private iVentaDAO dao;

	@Override
	public Venta registrar(Venta venta) {
		
		venta.getDetalleVenta().forEach(v -> v.setVenta(venta));
		return dao.save(venta);
	}

	@Override
	public Venta listarId(int idVenta) {
		return dao.findOne(idVenta);
	}

	@Override
	public List<Venta> listar() {
		return dao.findAll();
	}
	
}
