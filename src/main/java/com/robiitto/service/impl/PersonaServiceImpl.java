package com.robiitto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robiitto.dao.iPersonaDAO;
import com.robiitto.model.Persona;
import com.robiitto.service.iPersonaService;

@Service
public class PersonaServiceImpl implements iPersonaService{
	@Autowired
	private iPersonaDAO dao;

	@Override
	public Persona registrar(Persona persona) {
		return dao.save(persona);
		
	}

	@Override
	public void modificar(Persona persona) {
		this.registrar(persona);
		
	}

	@Override
	public void eliminar(int idPersona) {
		dao.delete(idPersona);
		
	}

	@Override
	public List<Persona> listar() {
		return dao.findAll();
	}

}
