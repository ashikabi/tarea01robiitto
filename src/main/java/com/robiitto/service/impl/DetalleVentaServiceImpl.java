package com.robiitto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robiitto.dao.iDetalleVentaDAO;
import com.robiitto.model.DetalleVenta;
import com.robiitto.service.iDetalleVentaService;

@Service
public class DetalleVentaServiceImpl implements iDetalleVentaService{

	@Autowired
	private iDetalleVentaDAO dao;

	@Override
	public DetalleVenta registrar(DetalleVenta detalleVenta) {
		
		return dao.save(detalleVenta);
	}

	@Override
	public DetalleVenta listarId(int idDetalleVenta) {
		return dao.findOne(idDetalleVenta);
	}

	@Override
	public List<DetalleVenta> listar() {
		return dao.findAll();
	}
	
	
	
}
