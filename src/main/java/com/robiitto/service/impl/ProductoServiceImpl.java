package com.robiitto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robiitto.dao.iProductoDAO;
import com.robiitto.model.Producto;
import com.robiitto.service.iProductoService;

@Service
public class ProductoServiceImpl implements iProductoService{
	
	@Autowired
	private iProductoDAO dao;

	@Override
	public Producto registrar(Producto producto) {
		return dao.save(producto);
		
	}

	@Override
	public void modificar(Producto producto) {
		this.registrar(producto);
		
	}

	@Override
	public void eliminar(int idProducto) {
		dao.delete(idProducto);
		
	}

	@Override
	public List<Producto> listar() {
		return dao.findAll();
	}

}
