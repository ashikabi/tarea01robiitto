package com.robiitto.service;

import java.util.List;

import com.robiitto.model.*;

public interface iPersonaService {

	Persona registrar(Persona persona);
	
	void modificar(Persona persona);
	
	void eliminar(int idPersona);
	
	List<Persona> listar();
	
}
