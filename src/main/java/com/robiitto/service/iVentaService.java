package com.robiitto.service;

import java.util.List;

import com.robiitto.model.*;

public interface iVentaService {

	Venta registrar(Venta venta);
	
	Venta listarId(int idVenta);
	
	List<Venta> listar();
	
}
