package com.robiitto.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.robiitto.model.Persona;
import com.robiitto.service.iPersonaService;

@RestController
@RequestMapping("/personas")
public class PersonaController {

	@Autowired
	private iPersonaService service;
	
	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Persona>> listar(){
		
		List<Persona> personas = new ArrayList<>();
		personas = service.listar();
		
		return new ResponseEntity<List<Persona>>(personas,HttpStatus.OK);
	}
	
	@PostMapping(value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Persona> registrar(@RequestBody Persona persona){
		Persona p = new Persona();
		try {
			p = service.registrar(persona);
		}catch(Exception e) {
			return new ResponseEntity<Persona>(p , HttpStatus.FAILED_DEPENDENCY);
		}
		
		return new ResponseEntity<Persona>(p , HttpStatus.OK);
	}
	
	@PutMapping(value = "/actualizar",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> actualizar(@RequestBody Persona persona){
		int resultado = 0; //cero es error
		
		try {
			service.modificar(persona);
			resultado=1;
		}catch(Exception e) {
			resultado=0;
		}
		
		return new ResponseEntity<Integer>(resultado , HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminar(@PathVariable("id") Integer id){
		int resultado = 0; //cero es error
		
		try {
			service.eliminar(id);
			resultado=1;
		}catch(Exception e) {
			resultado=0;
		}
		
		return new ResponseEntity<Integer>(resultado , HttpStatus.OK);
	}
	
}
