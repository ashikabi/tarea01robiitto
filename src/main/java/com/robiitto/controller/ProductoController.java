package com.robiitto.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.robiitto.service.iProductoService;
import com.robiitto.model.*;

@RestController
@RequestMapping("productos")
public class ProductoController {

	@Autowired
	private iProductoService service;
	
	@GetMapping(value = "/listar" , produces = "application/json")
	public ResponseEntity<List<Producto>> listar(){
		List<Producto> productos = new ArrayList<>();
		
		try {
			productos = service.listar();
		}catch(Exception e) {
			return new ResponseEntity<List<Producto>>(productos, HttpStatus.EXPECTATION_FAILED);
		}
		
		return new ResponseEntity<List<Producto>>(productos, HttpStatus.OK);
	}
	
	@PostMapping(value = "/registrar" , consumes = "application/json",produces = "application/json")
	public ResponseEntity<Producto> registrar(@RequestBody Producto producto){
		Producto p = new Producto();
		try {
			p = service.registrar(producto);
		}catch(Exception e) {
			return new ResponseEntity<Producto>(p, HttpStatus.EXPECTATION_FAILED);
		}
		
		return new ResponseEntity<Producto>(p, HttpStatus.OK);
	}
	
	@PutMapping(value = "/modificar" ,consumes = "application/json", produces = "application/json")
	public ResponseEntity<Integer> modificar(@RequestBody Producto producto){
		Integer resultado;
		try {
			service.modificar(producto);
			resultado = 1;
		}catch(Exception e) {
			resultado = 0;
			return new ResponseEntity<Integer>(resultado, HttpStatus.EXPECTATION_FAILED);
		}
		
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/eliminar/{id}" , produces = "application/json")
	public ResponseEntity<Integer> eliminar(@PathVariable("id")int id){
		Integer resultado;
		try {
			service.eliminar(id);
			resultado = 1;
		}catch(Exception e) {
			resultado = 0;
			return new ResponseEntity<Integer>(resultado, HttpStatus.EXPECTATION_FAILED);
		}
		
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}
	
}
