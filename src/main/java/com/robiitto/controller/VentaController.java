package com.robiitto.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.robiitto.model.*;
import com.robiitto.service.iVentaService;


@RestController
@RequestMapping("/ventas")
public class VentaController {
	
	@Autowired
	private iVentaService service;
	
	@GetMapping(value = "/listar" , produces = "application/json")
	public ResponseEntity<List<Venta>> listar(){
		List<Venta> ventas = new ArrayList<>();
		try {
			ventas = service.listar();
		}catch(Exception e) {
			return new ResponseEntity<List<Venta>>(ventas , HttpStatus.FAILED_DEPENDENCY);
		}
		return new ResponseEntity<List<Venta>>(ventas , HttpStatus.OK);
	}
	
	@PostMapping(value = "/registrar" ,consumes = "application/json", produces = "application/json")
	public ResponseEntity<Venta> registrar(@RequestBody Venta venta){
		Venta v = new Venta();
		try {
			v=service.registrar(venta);
		}catch(Exception e) {
			return new ResponseEntity<Venta>(v , HttpStatus.FAILED_DEPENDENCY);
		}
		return new ResponseEntity<Venta>(v , HttpStatus.OK);
	}

}
