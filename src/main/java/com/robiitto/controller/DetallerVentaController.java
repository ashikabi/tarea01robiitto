package com.robiitto.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.robiitto.model.DetalleVenta;
import com.robiitto.service.iDetalleVentaService;

@RestController
@RequestMapping("/detalleVentas")
public class DetallerVentaController {

	@Autowired
	private iDetalleVentaService service;
	
	@GetMapping(value = "/listar" , consumes= "application/json", produces = "application/json")
	public ResponseEntity<List<DetalleVenta>> listar(){
		List<DetalleVenta> detalles = new ArrayList<>();
				
				try {
					detalles = service.listar();
				}catch(Exception e){
					return new ResponseEntity<List<DetalleVenta>>(detalles,HttpStatus.FAILED_DEPENDENCY);
				}
		
		return new ResponseEntity<List<DetalleVenta>>(detalles,HttpStatus.OK);
				
	}
	
	@GetMapping(value = "/registrar" , consumes= "application/json", produces = "application/json")
	public ResponseEntity<DetalleVenta> registrar(@RequestBody DetalleVenta detalle){
				DetalleVenta det = new DetalleVenta();
				try {
					det = service.registrar(detalle);
				}catch(Exception e){
					return new ResponseEntity<DetalleVenta>(det,HttpStatus.FAILED_DEPENDENCY);
				}
		
		return new ResponseEntity<DetalleVenta>(det,HttpStatus.OK);
				
	}
	
}
