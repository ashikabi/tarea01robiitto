package com.robiitto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.robiitto.model.Persona;

@Repository
public interface iPersonaDAO extends JpaRepository<Persona, Integer>{

}
