package com.robiitto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.robiitto.model.*;
@Repository
public interface iDetalleVentaDAO extends JpaRepository<DetalleVenta, Integer>{

}
