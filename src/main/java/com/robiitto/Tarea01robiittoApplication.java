package com.robiitto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tarea01robiittoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tarea01robiittoApplication.class, args);
	}
}
